import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import time
from selenium.webdriver.support.ui import WebDriverWait

class scrap:

    def __init__(self,search):
        self.driver = webdriver.Firefox(executable_path="/home/pranav/geckodriver")
        self.search = search

    def test_search_in_python_org(self):
        driver = self.driver
        i=1
        driver.get("http://www.amazon.in")
        elem = driver.find_element_by_id('twotabsearchtextbox');
        elem.send_keys(self.search) #Element to search
        time.sleep(1)
        wait = WebDriverWait(driver,5)
        elem1 = wait.until(EC.element_to_be_clickable((By.XPATH,"/html/body/div[1]/header/div/div[1]/div[3]/div/form/div[2]/div/input")))
        elem1.click()
        time.sleep(1)
        page=[]   ##contains all the links on the page
        flag = ""
        if len(driver.find_elements_by_xpath("//ul[@id='s-results-list-atf' and @class='s-result-list s-col-1 s-col-ws-1 s-result-list-hgrid s-height-equalized s-list-view s-text-condensed s-item-container-height-auto']//a[@class='a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal']")) != 0 :
            flag = "list_view" #has list_view on the searched page
        elif len(driver.find_elements_by_xpath("//ul[@id='s-results-list-atf' and @class='s-result-list s-height-equalized s-image-view s-text-condensed s-col-4']//a[@class='a-link-normal s-access-detail-page s-overflow-ellipsis s-color-twister-title-link a-text-normal']")) != 0 :
            flag = "grid_view" #has grid_view on the searched page
        #else:
        #    quit()
        while True:
            try:
                if flag == "list_view":
                    product_url_list = [link.get_attribute('href') for link in driver.find_elements_by_xpath("//ul[@id='s-results-list-atf' and @class='s-result-list s-col-1 s-col-ws-1 s-result-list-hgrid s-height-equalized s-list-view s-text-condensed s-item-container-height-auto']//a[@class='a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal']")]
                else:
                    product_url_list = [link.get_attribute('href') for link in driver.find_elements_by_xpath("//ul[@id='s-results-list-atf' and @class='s-result-list s-height-equalized s-image-view s-text-condensed s-col-4']//a[@class='a-link-normal s-access-detail-page s-overflow-ellipsis s-color-twister-title-link a-text-normal']")]
                i = i + 1
                page.extend(product_url_list)
                #time.sleep(2)
                elem1 = driver.find_element_by_xpath("//a[@id='pagnNextLink']//span[@id='pagnNextString']")
                elem1.click()
                time.sleep(1.4)
            except:
                print("Toatal Pages:{0}".format(i))
                break
        return page


    def tearDown(self):
        driver.quit()
        pass

if __name__ == "__main__":
    obj = scrap("mouse")
    page = obj.test_search_in_python_org()
    obj.tearDown()
    print(page)
##//ul[@id='s-results-list-atf' and @class='s-result-list s-height-equalized s-image-view s-text-condensed s-col-4']//a[@class='a-link-normal s-access-detail-page s-overflow-ellipsis s-color-twister-title-link a-text-normal']
